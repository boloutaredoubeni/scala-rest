package models

case class Customer(id: Int, 
                    firstName: String,
                    lastName: String,
                    street: String,
                    city: String,
                    state: String,
                    // note: might be better as a Seq(Int)
                    zip: String,
                    country: String)

object Customer {
    
  
}